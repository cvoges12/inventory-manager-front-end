var canvas = document.querySelector('canvas');
var c = canvas.getContext('2d');

window.addEventListener('load', () => {
    resize();
})

window.addEventListener('resize', () => {
    resize();
});

function resize(){
    canvas.height = window.innerHeight;
    canvas.width = window.innerWidth;
}



// Rect
//c.fillStyle = 'rgba(255,0,255,0.5)';
//c.fillRect(100,200,100,100,0.5);
//c.fillStyle = 'rgba(0,0,255,0.5)';
//c.fillRect(300,400,100,100);
//c.fillStyle = 'rgba(0,255,0,0.5)';
//c.fillRect(400,100,100,100);

// Line
//c.beginPath();
//c.moveTo(50,300);
//c.lineTo(300,100);
//c.lineTo(400,200);
//c.strokeStyle = "fe3477";
//c.stroke();

// Arc
//c.beginPath();
//c.arc(300, 300, 30, 0.0, Math.PI, true);
//c.strokeStyle = 'blue';
//c.stroke();

// Circles
//for(var i = 100; i < (window.innerWidth - 100); i=i+40) {
//    var y = Math.random() * window.innerHeight;
//    c.beginPath();
//    c.arc(i, y, 5, 0.0, Math.PI * 2, true);
//    c.strokeStyle = 'red';
//    c.stroke();
//}

// Coil
function coil(x,y, width,od){
    var radius = od / 2;

    // bottom line
    c.beginPath();
    c.moveTo(x+width, y+od);
    c.lineTo(x,y+od);

    // left curve
    c.ellipse(x,y+radius, radius/4, radius, 0, 0.5 * Math.PI, 1.5 * Math.PI);

    // top line
    c.lineTo(x+width,y);
    c.strokeStyle = 'black';
    c.stroke();

    // right outer circle
    c.beginPath();
    c.ellipse(x+width,y+radius, radius/4, radius, 0, 0.5 * Math.PI, 3.5 * Math.PI);
    c.strokeStyle = 'black';
    c.stroke();

    // right inner circle
    c.beginPath();
    c.ellipse(x+width,y+radius, radius/8, radius/2, 0, 0.5 * Math.PI, 3.5 * Math.PI);
    c.strokeStyle = 'black';
    c.stroke();
}

const coils = {
    xs:     [],
    ys:     [],
    widths: [],
    ods:    []
};

const field = {
    xi: 0,
    xf: 12,
    yi: 0,
    yf: 12,
}

const initCoil = {
    x: 40,
    y: 50,
    width: 20,
    od: 30,
}

function initCoils(obj, f, r){
    for (var y=f.yi; y<f.yf; y++) {
        for (var x=f.xi; x<f.yf; x++) {
            var index = (y*f.xf) + x;

            coils.xs[index]       = (x + 1) * obj.x;
            coils.ys[index]       = (y + 1) * obj.y;
            coils.widths[index]   = obj.width   + (r * Math.random());
            coils.ods[index]      = obj.od      + (r * Math.random());
        }
    }
}

initCoils(initCoil,field,5);

function drawCoils(){
    for (var i=0; i < coils.xs.length; i++) {
        coil(
            coils.xs[i],
            coils.ys[i],
            coils.widths[i],
            coils.ods[i]
        );
    }
}

function drawZone(x, y, row, length){
    c.strokeRect(
        initCoil.x*(x+1) - 5,
        initCoil.y*(y+1) - 5,
        initCoil.x*row - 5,
        initCoil.y*length - 5);
}

function drawZones(){
    drawZone(0,0,6,6);
    drawZone(6,0,6,6);
    drawZone(0,6,6,6);
    drawZone(6,6,6,6);
}

function move(i, xf, yf){
    if (coils.xs[i] > initCoil.x*(xf+1)) coils.xs[i]--;
    if (coils.xs[i] < initCoil.x*(xf+1)) coils.xs[i]++;
    if (coils.ys[i] > initCoil.y*(yf+1)) coils.ys[i]--;
    if (coils.ys[i] < initCoil.y*(yf+1)) coils.ys[i]++;
}

function movements(){
    move(0,2,13);
    if ((coils.xs[0] == initCoil.x*(2+1)) 
        && (coils.ys[0] == initCoil.y*(13+1))) {
        move(2,0,0);
        if ((coils.xs[2] == initCoil.x*(0+1)) 
            && (coils.ys[2] == initCoil.y*(0+1))) {
            move(0,2,0);
        }
    }
}

function animate(){
    requestAnimationFrame(animate);
    c.clearRect(0,0,innerWidth,innerHeight);

    drawCoils();
    drawZones();

    movements();
    //move(0,2,0);
}

animate();
